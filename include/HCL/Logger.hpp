#ifndef HCL_LOGGER_HPP
#define HCL_LOGGER_HPP

#include <QByteArray>
#include <QFile>
#include <QString>
#include <QObject>

namespace Hcl
{
    class Logger : public QObject
    {
        Q_OBJECT

        private:
            QFile _file;
            QByteArray _tmp;
            void _init();

        public:
            static const char* Black;
            static const char* Red;
            static const char* Green;
            static const char* Blue;
            static const char* Cyan;
            static const char* Magenta;
            static const char* Yellow;
            static const char* Normal;
            static const char* Grey;
            static const char* LightRed;
            static const char* LightGreen;
            static const char* LightBlue;
            static const char* LightCyan;
            static const char* LightMagenta;
            static const char* LightYellow;
            static const char* White;

            explicit Logger(QString path, QObject *parent = 0);
            explicit Logger(QObject *parent = 0);
            virtual ~Logger();
            // Write a string to the log file
            void write(QString, QString color = "\e[0m");
            // Move log to another file
            // Write all buffered data if no file was opened
            bool move(QString);
    };
}

#endif // HCL_LOGGER_HPP
