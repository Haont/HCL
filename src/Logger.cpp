#include "include/HCL/Logger.hpp"

// C++ STL
#include <iostream>
// Qt
#include <QCoreApplication>
#include <QDateTime>
#include <QTime>

namespace Hcl
{
    const char* Logger::Black        = "\e[0;30m"; // Do not use
    const char* Logger::Red          = "\e[0;31m";
    const char* Logger::Green        = "\e[0;32m";
    const char* Logger::Blue         = "\e[0;33m";
    const char* Logger::Cyan         = "\e[0;34m";
    const char* Logger::Magenta      = "\e[0;35m";
    const char* Logger::Yellow       = "\e[0;36m";
    const char* Logger::Normal       = "\e[0;37m";
    const char* Logger::Grey         = "\e[1;30m";
    const char* Logger::LightRed     = "\e[1;31m";
    const char* Logger::LightGreen   = "\e[1;32m";
    const char* Logger::LightYellow  = "\e[1;33m";
    const char* Logger::LightBlue    = "\e[1;34m";
    const char* Logger::LightMagenta = "\e[1;35m";
    const char* Logger::LightCyan    = "\e[1;36m";
    const char* Logger::White        = "\e[1;37m";

    Logger::Logger(QString path, QObject *parent) : QObject(parent)
    {
        move(path);
        _init();
    }

    Logger::Logger(QObject *parent) : QObject(parent)
    {
        _init();
    }

    Logger::~Logger()
    {
        write("=======================", LightYellow);
        write("Application is stopping",  White);
        write("=======================", LightYellow);
    }

    void Logger::_init()
    {
        write("=======================",                                                           LightYellow);
        write(QString("Application \"%1\"").arg(QCoreApplication::applicationName()),     LightGreen);
        if(!QCoreApplication::applicationVersion().isEmpty())
            write(QString("Version: %1").arg(QCoreApplication::applicationVersion()),               White);
        write(QString("Initiated at %2").arg(QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss t")), White);
        write("=======================",                                                           LightYellow);
    }

    void Logger::write(QString data, QString color)
    {
        QString time = QTime::currentTime().toString("HH:mm:ss:zzz");
        QByteArray out = QString("[%1] %2%3\n").arg(time).arg(data).toUtf8();

        // stdout
        std::cout << '[' << time.toStdString() << "] ";
#ifndef _WIN32
        std::cout << color.toStdString();
#endif
        std::cout << data.toStdString();
#ifndef _WIN32
        std::cout << "\e[0m\n";
#endif
        std::cout.flush();
        if(_file.isOpen())
        {
            _file.write(out);
            _file.flush();
        }
        else
        {
            _tmp.append(out);
        }
    }

    bool Logger::move(QString path)
    {
        if(_file.isOpen())
        {
            if(_file.rename(path))
            {
                write(QString("Logfile moved to %1").arg(_file.fileName()));
                return true;
            }
        }
        else
        {
            _file.setFileName(path);
            _file.open(QFile::WriteOnly);
            if(_file.isOpen() && _file.write(_tmp) == _tmp.size())
            {
                _tmp.clear();
                write(QString("Logfile saved to %1").arg(_file.fileName()));
                return true;
            }
        }
        write(QString("Failed to save logfile to %1").arg(_file.fileName()));
        return false;
    }
}
