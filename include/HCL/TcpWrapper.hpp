#ifndef HCL_TCPWRAPPER_HPP
#define HCL_TCPWRAPPER_HPP

// C
#include <stdint.h>
// Qt
#include <QByteArray>
#include <QObject>
#include <QTcpSocket>
#include <QTimer>
#include <QQueue>

#ifndef HCL_DEFAULT_TIMEOUT
#define HCL_DEFAULT_TIMEOUT 5000
#endif

// TODO: Ping

namespace Hcl
{
    class TcpWrapper : public QObject
    {
        Q_OBJECT

        enum Status
        {
            DISCONNECTED,
            FINDING_HOST,
            HOST_NOT_FOUND,
            HOST_FOUND,
            TIMED_OUT,
            CONNECTED
        };

        private:
            QTcpSocket*      _socket;
            QQueue<uint16_t> _queue;
            QTimer           _timer;
            // If we have read the pending package size, but haven't receive the package itself, we store it's size here
            bool             _isReceiving;
            uint16_t         _pending;
            uint16_t         _packageCounter;
            uint16_t         _timeout;
            Status           _status;
            bool             _ownSocket;

            void _tryReadHeader();
            void _tryReadData();

        public:
            explicit TcpWrapper(QTcpSocket*, QObject *parent = 0);
            explicit TcpWrapper(QObject *parent = 0);
            virtual ~TcpWrapper();

            void setSocket(QTcpSocket*);
            Status status();
            QTcpSocket::SocketState state();

        private slots:
            // This slot is called when the timer ticks
            // This should receive all the data needed
            void _tick();
            void _hostFound();
            void _connected();
            void _disconnected();

        public slots:
            // The wrapper uses a timer, this sets up it's interval (in msecs)
            // Note: zero value will actually set 1 to the timer
            // Note: affects ping value (set 1 for the most accurate checking)
            void setInterval(uint16_t);
            // Reading timeout, in msecs
            // If the package was not received in time, the socket will be closed
            // Note: do not set the timeout less than timer interval
            // Recommended value is 5 seconds
            void setTimeout(uint16_t);
            // Asynchronous data receiving request; The value is data size limit
            void requestData(uint16_t = 0xffff);
            void    sendData(QByteArray);
            // Send all data immediately
            // Note: it may take a very long time
            void flush();
            void connectToHost(const QHostAddress&, uint16_t/*, OpenMode mode = ReadWrite*/);
            void disconnectFromHost();

        signals:
            void dataReceived(QByteArray);
            void hostFound();
            void connected();
            void disconnected();
            void timedOut();
            void notFound();
    };
}

#endif // HCL_TCPWRAPPER_HPP
