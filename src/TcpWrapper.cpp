#include "../include/HCL/TcpWrapper.hpp"

// Just for debug purpose
#include <iostream>

namespace Hcl
{
    TcpWrapper::TcpWrapper(QTcpSocket* socket, QObject *parent) : QObject(parent)
    {
        _isReceiving = false;
        _timeout = HCL_DEFAULT_TIMEOUT;
        setSocket(socket);
        _status = CONNECTED;
        connect(&_timer, &QTimer::timeout, this, &TcpWrapper::_tick);
        _timer.start(1);
        _ownSocket = false;
    }

    TcpWrapper::TcpWrapper(QObject *parent) : QObject(parent)
    {
        _isReceiving = false;
        _timeout = HCL_DEFAULT_TIMEOUT;
        _socket = NULL;
        _status = DISCONNECTED;
        connect(&_timer, &QTimer::timeout, this, &TcpWrapper::_tick);
        _timer.start(1);
        _ownSocket = false;
    }

    TcpWrapper::~TcpWrapper()
    {
        disconnectFromHost();
    }

    void TcpWrapper::_tryReadHeader()
    {
        if(!_queue.isEmpty())
        {
            if(_socket && _socket->isReadable() && _socket->bytesAvailable() >= 2)
            {
                if(_socket->read((char*)(&_pending), 2) == 2 && _pending <= _queue.first())
                {
                    _queue.pop_front();
                    _isReceiving = true;
                }
                else
                {
                    disconnectFromHost();
                }
            }
        }
    }

    void TcpWrapper::_tryReadData()
    {
        if(_socket && _socket->isReadable() && _socket->bytesAvailable() >= _pending)
        {
            QByteArray buf = _socket->read(_pending);
            _isReceiving = false;
            emit dataReceived(buf);
        }
        else
        {
            if(_packageCounter * _timer.interval() < _timeout)
            {
                _status = TIMED_OUT;
                emit timedOut();
                disconnectFromHost();
            }
        }
    }

    void TcpWrapper::_tick()
    {
        if(_socket && (_status == FINDING_HOST || _status == HOST_FOUND))
        {
            ++_packageCounter;
            if(_packageCounter * _timer.interval() >= _timeout)
            {
                _socket->disconnectFromHost();
                switch(_status)
                {
                    case FINDING_HOST: _status = HOST_NOT_FOUND; emit notFound(); break;
                    case   HOST_FOUND: _status = TIMED_OUT;      emit timedOut(); break;
                    default: std::cout << "TcpWrapper::_status is probably corrupted (value is " << _status << ")\n"; break;
                }
            }
        }
        if(_socket && _socket->isReadable() &&_status == CONNECTED)
        {
            while(_isReceiving)
            {
                _tryReadData();
                _tryReadHeader();
            }
            _tryReadHeader();
        }
    }

    void TcpWrapper::requestData(uint16_t size)
    {
        _queue.push_back(size);
        _tick();
    }

    void TcpWrapper::setSocket(QTcpSocket* socket)
    {
        disconnectFromHost();
        _socket = socket;
        _ownSocket = false;
        if(socket)
        {
            _status = CONNECTED;
            _packageCounter = 0;
            connect(_socket, &QTcpSocket::hostFound,    this,    &TcpWrapper::_hostFound);
            connect(_socket, &QTcpSocket::   connected, this,    &TcpWrapper::_connected);
            connect(_socket, &QTcpSocket::disconnected, this,    &TcpWrapper::_disconnected);
            _timer.start();
        }
    }

    void TcpWrapper::setInterval(uint16_t interval)
    {
        _timer.setInterval(interval);
    }

    void TcpWrapper::setTimeout(uint16_t timeout)
    {
        _timeout = timeout;
    }

    void TcpWrapper::flush()
    {
        if(_socket && _status == CONNECTED)
            _socket->flush();
    }

    void TcpWrapper::connectToHost(const QHostAddress& address, uint16_t port/*, OpenMode mode*/)
    {
        QTcpSocket* newsocket = new QTcpSocket;
        setSocket(newsocket);
        _socket->connectToHost(address, port/*, mode*/);
        _status = FINDING_HOST;
        _timer.start();
        _tick();
        _ownSocket = true;
    }

    void TcpWrapper::disconnectFromHost()
    {
        //_timer.stop();
        _queue.clear();
        _isReceiving = false;
        _pending = 0;
        _packageCounter = 0;
        if(_socket && _status != DISCONNECTED)
        {
            _socket->disconnectFromHost();
            //_socket->deleteLater();
            if(_ownSocket)
            {
                _socket = NULL;
                _ownSocket = false;
            }
        }
        _status = DISCONNECTED;
    }

    void TcpWrapper::_hostFound()
    {
        _packageCounter = 0;
        _status = HOST_FOUND;
        emit hostFound();
    }

    void TcpWrapper::_connected()
    {
        _packageCounter = 0;
        _status = CONNECTED;
        emit connected();
    }

    void TcpWrapper::_disconnected()
    {
        if(_socket && _status != DISCONNECTED && _status != TIMED_OUT && _status != HOST_NOT_FOUND)
        {
            //_socket->deleteLater();
            _status = DISCONNECTED;
            emit disconnected();
        }
    }

    void TcpWrapper::sendData(QByteArray data)
    {
        if(_socket && _status == CONNECTED)
        {
            uint16_t len = data.size();
            _socket->write((char*)(&len), 2);
            _socket->write(data);
        }
    }
}
